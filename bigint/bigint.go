package bigint

import (
	"fmt"
	"strconv"
	"strings"
)

type Bigint struct {
	Value string
}

func NewInt(num string) (Bigint, error) {

	return Bigint{
		Value: num,
	}, nil
}

func (z *Bigint) Set(num string) error {
	//
	// Validate input
	//
	z.Value = num
	return nil
}

func Add(a, b Bigint) Bigint {
	num1, num2 := FillWithZeros(a.Value, b.Value)
	val1, val2 := strings.Split(num1, ""), strings.Split(num2, "")

	//	VALIDATION GOES HERE
	if !(ValidateNum(num1) && ValidateNum(num2)) {
		return Bigint{Value: "Error: check your values"}
	}
	// END OATION

	i, j := len(num1)-1, len(num2)-1
	carry := 0

	var result []string
	var d1 int
	var d2 int

	for i > -1 || j > -1 {

		d1, _ = strconv.Atoi(val1[i])
		d2, _ = strconv.Atoi(val2[j])

		sum := d1 + d2 + carry
		quotient := sum % 10
		result = append(result, fmt.Sprint(quotient))
		carry = sum / 10
		i--
		j--
	}
	if carry == 1 {
		result = append(result, fmt.Sprint(carry))
	}

	res := trimZeros(Reverse(strings.Join(result, "")))
	return Bigint{Value: res}
}

func Sub(a, b Bigint) Bigint {
	num1, num2 := FillWithZeros(a.Value, b.Value)
	val1, val2 := strings.Split(num1, ""), strings.Split(num2, "")

	//	VALIDATION GOES HERE
	if !(ValidateNum(num1) && ValidateNum(num2)) {
		return Bigint{Value: "Error: check your values"}
	}
	// END OF VALIDATION

	if num2 > num1 {
		ret := Sub(b, a)
		return Bigint{Value: "-" + ret.Value}
	}

	i, j := len(num1)-1, len(num2)-1
	carry := 0

	var result []string
	var d1 int
	var d2 int
	var sum int

	for i > -1 || j > -1 {

		d1, _ = strconv.Atoi(val1[i])
		d2, _ = strconv.Atoi(val2[j])

		if carry != 0 {
			d1 -= carry
			carry = 0
		}
		if d1 >= d2 {
			sum = d1 - d2
		} else {
			sum = 10 + (d1 - d2)
			carry = 1
		}
		result = append([]string{strconv.Itoa(sum)}, result...)

		i--
		j--
	}

	res := trimZeros(strings.Join(result, ""))
	return Bigint{Value: res}

}

func Multiply(a, b Bigint) Bigint {
	num1, num2 := FillWithZerosForMul(a.Value, b.Value)
	val1, val2 := strings.Split(num1, ""), strings.Split(num2, "")

	i, j := len(num1)-1, len(num2)-1
	carry := 0

	var result []string
	var d1 int
	var d2 int

	for i > -1 || j > -1 {

		d1, _ = strconv.Atoi(val1[i])
		d2, _ = strconv.Atoi(val2[j])
		fmt.Println("carry ", carry)
		sum := (d1 * d2) + carry
		quotient := sum % 10
		result = append(result, fmt.Sprint(quotient))
		carry = sum / 10
		fmt.Printf("d1: %v d2: %v sum: %v\nquotient: %v\nresult: %v\n", d1, d2, sum, quotient, result)
		i--
		j--
	}
	fmt.Println("carry ", carry)
	if carry > 0 {
		result = append(result, fmt.Sprint(carry))
	}

	res := trimZeros(Reverse(strings.Join(result, "")))
	return Bigint{Value: res}

}

func Mod(a, b Bigint) Bigint {
	//
	// MATH Mod
	//
	return Bigint{Value: ""}
}

// Helper Functions

func Reverse(s string) string {
	/*
		Hello Read this
	*/
	lenS := len(s)
	str := []rune(s)

	for i, j := 0, lenS-1; i < j; {
		str[i], str[j] = str[j], str[i]
		i++
		j--
	}

	return string(str)
}
func FillWithZeros(a, b string) (string, string) {
	lenA := len(a)
	lenB := len(b)

	revA := a
	revB := b
	switch {
	case lenA > lenB:
		dif := lenA - lenB
		zeros := ""
		for i := 0; i < dif; i++ {
			zeros += "0"
		}
		revB = zeros + revB

	case lenA < lenB:
		dif := lenB - lenA
		zeros := ""
		for i := 0; i < dif; i++ {
			zeros += "0"
		}
		revA = zeros + revA
	}
	return revA, revB
}
func FillWithZerosForMul(a, b string) (string, string) {
	lenA := len(a)
	lenB := len(b)

	revA := a
	revB := b
	switch {
	case lenA > lenB:
		dif := lenA - lenB
		zeros := ""
		for i := 0; i < dif; i++ {
			zeros += "1"
		}
		revB = zeros + revB

	case lenA < lenB:
		dif := lenB - lenA
		zeros := ""
		for i := 0; i < dif; i++ {
			zeros += "1"
		}
		revA = zeros + revA
	}
	return revA, revB
}
func trimZeros(s string) string {
	for strings.HasPrefix(s, "0") && len(s) > 1 {
		s = strings.Replace(s, "0", "", 1)
	}
	return s
}
func Positive(n int) int {
	if n < 0 {
		return -(n)
	}
	return n
}
func ValidateNum(num string) bool {
	x := []rune(num)
	z := 0
	if string(x[0]) == "-" || string(x[0]) == "+" {
		z = 1
	}
	for i := z; i < len(num); i++ {
		if !(string(x[i]) >= "0" && string(x[i]) <= "9") {
			return false
		}
	}
	return true
}
